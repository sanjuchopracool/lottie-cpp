TEMPLATE = subdirs

CONFIG += c++14

SUBDIRS += \
    LibDriver \
    lottie-lib

LibDriver.depends = lottie-lib

CONFIG += qt

QT += widgets
