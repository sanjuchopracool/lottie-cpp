#ifndef LOTTIELIB_H
#define LOTTIELIB_H

namespace Lottie
{
    using FrameType = double;

enum class CoordinateSpace
{
    Type2D,
    Type3D,
};
}

#endif // LOTTIELIB_H
