#-------------------------------------------------
#
# Project created by QtCreator 2019-08-03T17:53:29
#
#-------------------------------------------------

QT       += gui widgets

TARGET = lottie-lib
TEMPLATE = lib
CONFIG += staticlib
CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AnimationWidget.cpp \
    src/Private/LayerContainers/AnimationContainer.cpp \
    src/Private/LayerContainers/AnimationNodes/AnimationNodeFactory.cpp \
    src/Private/LayerContainers/AnimationNodes/EllipseNode.cpp \
    src/Private/LayerContainers/AnimationNodes/ShapeTransformationNode.cpp \
    src/Private/LayerContainers/AnimationNodes/StrokeNode.cpp \
    src/Private/LayerContainers/AnimationNodes/TransformationNode.cpp \
    src/Private/LayerContainers/AnimationNodes/TrimNode.cpp \
    src/Private/LayerContainers/CompositionLayer.cpp \
    src/Private/LayerContainers/NullCompositionLayer.cpp \
    src/Private/LayerContainers/TransformedPaintBuffer.cpp \
    src/Private/LayerContainers/CompositionLayerFactory.cpp \
    src/Private/LayerContainers/AnimationNodes/FillNode.cpp \
    src/Private/LayerContainers/AnimationNodes/GroupNode.cpp \
    src/Private/LayerContainers/ShapeCompositionLayer.cpp \
    src/Private/LayerContainers/AnimationNodes/ShapeNode.cpp \
    src/Private/Model/Animation.cpp \
    src/Private/Model/Layers/LayerFactory.cpp \
    src/Private/Model/Layers/LayerModel.cpp \
    src/Private/Model/Layers/ShapeLayer.cpp \
    src/Private/Model/ShapeItems/Ellipse.cpp \
    src/Private/Model/ShapeItems/Fill.cpp \
    src/Private/Model/ShapeItems/Stroke.cpp \
    src/Private/Model/ShapeItems/Group.cpp \
    src/Private/Model/ShapeItems/Merge.cpp \
    src/Private/Model/ShapeItems/Shape.cpp \
    src/Private/Model/ShapeItems/ShapeFactory.cpp \
    src/Private/Model/ShapeItems/ShapeItem.cpp \
    src/Private/Model/ShapeItems/ShapeTransformation.cpp \
    src/Private/Model/ShapeItems/Trim.cpp \
    src/Private/Utility/Primitives/BezierPath.cpp \
    src/Private/Utility/Primitives/CurveVertex.cpp \
    src/Private/Utility/Primitives/DashElement.cpp \
    src/Private/Utility/Primitives/PathElement.cpp \
    src/Private/Utility/Primitives/Transformation.cpp

HEADERS += \
        AnimationWidget.h \
        lottielib.h \
        src/Private/AutoProfiler.h \
        src/Private/LayerContainers/AnimationContainer.h \
        src/Private/LayerContainers/AnimationNodes/AnimationNodeFactory.h \
        src/Private/LayerContainers/AnimationNodes/EllipseNode.h \
        src/Private/LayerContainers/AnimationNodes/ShapeNodeInterface.h \
        src/Private/LayerContainers/AnimationNodes/ShapeTransformationNode.h \
        src/Private/LayerContainers/AnimationNodes/StrokeNode.h \
        src/Private/LayerContainers/AnimationNodes/TransformationNode.h \
        src/Private/LayerContainers/AnimationNodes/TrimNode.h \
        src/Private/LayerContainers/CompositionLayer.h \
        src/Private/LayerContainers/NullCompositionLayer.h \
        src/Private/LayerContainers/TransformedPaintBuffer.h \
        src/Private/LayerContainers/CompositionLayerFactory.h \
        src/Private/LayerContainers/AnimationNodes/FillNode.h \
        src/Private/LayerContainers/AnimationNodes/GroupNode.h \
        src/Private/LayerContainers/ShapeCompositionLayer.h \
        src/Private/LayerContainers/AnimationNodes/ShapeNode.h \
        src/Private/Model/Animation.h \
        src/Private/Model/Keyframes/KeyFrame.h \
        src/Private/Model/Keyframes/KeyFrameGroup.h \
        src/Private/Model/Layers/LayerFactory.h \
        src/Private/Model/Layers/LayerModel.h \
        src/Private/Model/Layers/ShapeLayer.h \
        src/Private/Model/ShapeItems/Ellipse.h \
        src/Private/Model/ShapeItems/Fill.h \
        src/Private/Model/ShapeItems/Stroke.h \
        src/Private/Model/ShapeItems/Group.h \
        src/Private/Model/ShapeItems/Merge.h \
        src/Private/Model/ShapeItems/Shape.h \
        src/Private/Model/ShapeItems/ShapeFactory.h \
        src/Private/Model/ShapeItems/ShapeItem.h \
        src/Private/Model/ShapeItems/ShapeTransformation.h \
        src/Private/Model/ShapeItems/Trim.h \
        src/Private/NodeRenderSystem/NodeProperties/AnyValueContainer.h \
        src/Private/NodeRenderSystem/NodeProperties/AnyValueProvider.h \
        src/Private/NodeRenderSystem/NodeProperties/NodeProperty.h \
        src/Private/NodeRenderSystem/NodeProperties/ValueProviders/KeyFrameValueProvider.h \
        src/Private/NodeRenderSystem/NodeProperties/ValueProviders/SingleValueProvider.h \
        src/Private/NodeRenderSystem/NodeProperties/keypathsearchable.h \
        src/Private/Utility/Interpolatable/Interpolatable.h \
        src/Private/Utility/Primitives/BezierPath.h \
        src/Private/Utility/Primitives/CurveVertex.h \
        src/Private/Utility/Primitives/DashElement.h \
        src/Private/Utility/Primitives/PathElement.h \
        src/Private/Utility/Primitives/Transformation.h \
        src/Private/Utility/Primitives/Utility.h \
        src/Private/Utility/Primitives/Vector1D.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    DESIGN_CHOICES.txt \
    Implementation.txt \
    lottie-lib.pri \
    src/Private/LayerContainers/doc.txt
