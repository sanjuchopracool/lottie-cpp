SOURCES += \
    $$PWD/src/Private/Model/Animation.cpp

HEADERS += \
        $$PWD/lottielib.h \
        $$PWD/src/Private/Model/Animation.h \
        $$PWD/src/Private/NodeRenderSystem/NodeProperties/AnyValueContainer.h \
        $$PWD/src/Private/NodeRenderSystem/NodeProperties/AnyValueProvider.h \
        $$PWD/src/Private/NodeRenderSystem/NodeProperties/NodeProperty.h \
        $$PWD/src/Private/NodeRenderSystem/NodeProperties/ValueProviders/SingleValueProvider.h \
        $$PWD/src/Private/Utility/Interpolatable/Interpolatable.h
