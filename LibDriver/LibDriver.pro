TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += \
        main.cpp

INCLUDEPATH += $$PWD/../lottie-lib
DEPENDPATH += $$PWD/../lottie-lib



unix: LIBS += -L$$OUT_PWD/../lottie-lib/ -llottie-lib

INCLUDEPATH += $$PWD/../lottie-lib
DEPENDPATH += $$PWD/../lottie-lib

unix: PRE_TARGETDEPS += $$OUT_PWD/../lottie-lib/liblottie-lib.a
